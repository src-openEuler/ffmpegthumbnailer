Name:           ffmpegthumbnailer
Version:        2.2.3
Release:        3
Summary:        Lightweight video thumbnailer that can be used by file managers

License:        GPL-2.0-or-later
URL:            https://github.com/dirkvdb/%{name}
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
Patch0:         add_audio_cover.patch
Patch1:         gcc12_fix.patch

BuildRequires:  ffmpeg-devel
BuildRequires:  libpng-devel
BuildRequires:  libjpeg-devel
BuildRequires:  cmake
BuildRequires:  gcc-c++


%description
This video thumbnailer can be used to create thumbnails for your video files.

%package devel
Summary:        Headers and libraries for building apps that use ffmpegthumbnailer
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This video thumbnailer can be used to create thumbnails for your video files,
development package.

%prep
%autosetup -p1
chmod -x README INSTALL COPYING AUTHORS

%build
%cmake -DENABLE_GIO=ON -DENABLE_THUMBNAILER=ON
%cmake_build

 
%install
%cmake_install

%files
%doc README AUTHORS
%license COPYING
%{_bindir}/ffmpegthumbnailer
%{_libdir}/libffmpegthumbnailer.so.4*
%{_mandir}/man1/ffmpegthumbnailer.1*
# gnome thumbnailer registration
%dir %{_datadir}/thumbnailers
%{_datadir}/thumbnailers/ffmpegthumbnailer.thumbnailer

%files devel
%{_libdir}/libffmpegthumbnailer.so
%{_libdir}/pkgconfig/libffmpegthumbnailer.pc
%{_includedir}/libffmpegthumbnailer/


%changelog
* Sun Mar 09 2025 Funda Wang <fundawang@yeah.net> - 2.2.3-3
- buildrequires cmake rather than cmake3

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 2.2.3-2
- adopt to new cmake macro

* Mon Mar 11 2024 peijiankang <peijiankang@kylinos.cn> - 2.2.3-1
- update to 2.2.3 to fix build error about ffmpeg-6.1

* Thu May 13 2021 He Rengui <herengui@uniontech.com> - 2.2.2-1
- package init
